export const SCREEN_SIZES = [
    {
        name: 'mobile portrait',
        width: 320
    },
    {
        name: 'mobile landscape',
        width: 480
    },
    {
        name: 'ipad portrait',
        width: 768
    },
    {
        name: 'browser, ipad landscape',
        width: 1024
    },
    {
        name: 'browser',
        width: 1200
    },
    {
        name: 'browser',
        width: 1600
    }
]