
import puppeteer from 'puppeteer'
import { SCREEN_SIZES } from './config'

let browser
let page

beforeAll(async () => {
    browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage'
        ]
    });

    page = await browser.newPage();
    await page.goto(`https://sytac.io/`); 
});

it('generate screenshots', async () => {
    for (let i=0; i < SCREEN_SIZES.length; i++) {
        const screen = SCREEN_SIZES[i]
        await page.setViewport({ width: screen.width, height: 200 });
        await page.screenshot({
            path: `./screenshots/${screen.name}_${screen.width}.jpg`,
            type: "jpeg",
            fullPage: true
        }); 
    }
});

afterAll(async () => {
  await browser.close();
});